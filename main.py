import numpy as np
from EM import Gmm
from EM import Kmeans


def main():

    data = np.loadtxt('./GMM_dataset.txt', delimiter=",")

    for k in [2, 3, 4, 5]:
        best = Kmeans.run_r_times(10, k, data)
        best.plot_color(20)
        print("K " + str(best.k))
        print("means " + str(best.means))
        print("error " + str(best.error))

    for k in [2, 3, 4, 5]:
        best = Gmm.run_r_iterations(10, k, data)
        best.plot_color("K" + str(k))
        print("K " + str(best.k))
        print("MU "+str(best.mu))
        print("SIGMA " + str(best.sigma))
        print("COEF " + str(best.mix_coef))
        print("LOG " + str(best.prev_log))


if __name__ == "__main__":
    main()
